# Log In Game

The plugin use Smod2 Framework. 
A simple plugin to print log in game for your moderation team !

## Installation
Extract the archive in sm_plugins.

## Commands

To hide the log in the console, execute the command `hidelog` or `hl`.

## Config : Global

**Type Info:**

-   `List`: A list with items separated by ",", for example: `list: 1,2,3,4,5`
-   `Bool`: True or False value
-   `String` : Character Chain value

| Config Option      |     Value Type    |   Default Value | Description
| ------ | ------ | ------ | ------ |
| `lig_rank`     | List | owner | Set rank able to show log
| `lig_steamid` | List | Empty | Set player able to show log
| `lig_steamid_badge` | Bool | True | Add a badge to the players who are allowed to show the log
| `lig_steamid_badge_name` | String | The Log Shower | Set the badge name
| `lig_steamid_badge_color` | Bool | red | Set the badge color

## Config : In Game

**Type Info:**

-   `List`: A list with items separated by ",", for example: `list: 1,2,3,4,5`
-   `Bool`: True or False value

### Other

| Config Option      |     Value Type    |   Default Value | Description |
| ------ | ------ | ------ | ------ |
| `lig_log_teamrespawn` | Bool | true | Print log relative to TeamRespawn |
| `lig_log_intercom` | Bool | true | Print log relative to Intercom |
| `lig_log_roundstart` | Bool | true | Print log relative to RoundStart |
| `lig_log_roundend` | Bool | true | Print log relative to RoundEnd |
| `lig_log_lczdecont` | Bool | true | Print log relative to LCZ Decontamination |
| `lig_log_decide_team_respawn` | Bool | true | Print log relative to the Decide Team Respawn Queue |

### WarHead
| Config Option      |     Value Type    |   Default Value | Description |
| ------ | ------ | ------ | ------ |
| `lig_log_warhead_start` | Bool | true | Print log when the WarHead's countdown is start |
| `lig_log_warhead_stop` | Bool | true | Print log when the WarHead's countdown is stop |
| `lig_log_warhead_explode` | Bool | true | Print log when the WarHead explode |

### Player

| Config Option      |     Value Type    |   Default Value | Description |
| ------ | ------ | ------ | ------ |
| `lig_log_player_connect` | Bool | true | Print log when a player is connecting |
| `lig_log_player_join` | Bool | true | Print log when a player join the server |
| `lig_log_player_disconnect` | Bool | true | Print log when a player leave the server |
| `lig_log_player_kill` | Bool | true | Print log when a player die |
| `lig_log_player_grenade` | Bool | true | Print log when a player threw a grenade |
| `lig_log_player_command` | Bool | true | Print log when an admin execute a command |
| `lig_log_player_ban` | Bool | true | Print log when someone is ban |
| `lig_log_player_handcuff` | Bool | true | Print log when a player handcuff someone |
| `lig_log_player_elevator` | Bool | true | Print log when a player use an elevator |
| `lig_log_player_escape` | Bool | true | Print log when a player escape from the facility |
| `lig_log_player_door` | Bool | false | Print log when a player open a door |
| `lig_log_player_secure_door` | Bool | true | Print log when a player open a door requiring a keycard |
| `lig_log_player_authcheck` | Bool | true | Print log when a player connect to the RA Panel |
| `lig_log_player_spawn` | Bool | true | Print log when a player spawn |
| `lig_log_player_zombie`| Bool | true | Print log when a player become a Zombie |
| `lig_log_player_callcommand`| Bool | true | Print log when a player call a command with the `ù` console |

### Pocket Dimension

| Config Option      |     Value Type    |   Default Value | Description |
| ------ | ------ | ------ | ------ |
| `lig_log_pd_enter` | Bool | true | Print log when someone enter to the Pocket dimension |
| `lig_log_pd_exit` | Bool | true | Print log when someone exit from the Pocket dimension | 
| `lig_log_pd_die` | Bool | true | Print log when someone die inside the Pocket dimension |

### 106 Containment
| Config Option      |     Value Type    |   Default Value | Description |
| ------ | ------ | ------ | ------ |
| `lig_log_lure` | Bool | true | Print log when someone enter to the Femur Breaker |
| `lig_log_contain_106` | Bool | true | Print log when someone contain SCP 106 with the Femur Breaker |

### 079

| Config Option      |     Value Type    |   Default Value | Description |
| ------ | ------ | ------ | ------ |
| `lig_log_079_door` | Bool | false | Print log when 079 open/close a door |
| `lig_log_079_lock` | Bool | false | Print log when 079 lock a door |
| `lig_log_079_elevator` | Bool | false | Print log when 079 call an elevator |
| `lig_log_079_teslagate` | Bool | false | Print log when 079 use a Tesla Gate |
| `lig_log_079_addexp` | Bool | false | Print log when 079 get XP. |
| `lig_log_079_levelup` | Bool | false | Print log when 079 level up. |
| `lig_log_079_unlockdoors` | Bool | false | Print log when 079 unlock doors. |
| `lig_log_079_camerateleport` | Bool | false | Print log when 079 teleport himself to a camera |
| `lig_log_079_startspeaker` | Bool | false | Print log when 079 begin to speak |
| `lig_log_079_stopspeaker` | Bool | false | Print log when 079 finish to speak |

### Generator
| Config Option      |     Value Type    |   Default Value | Description |
| ------ | ------ | ------ | ------ |
| `lig_log_generator_access` | Bool | true | Print log when someone attempt to open/close a generator |
| `lig_log_generator_ejecttablet` | Bool | true | Print log when someone eject a tablet |
| `lig_log_generator_finish` | Bool | true | Print log when a generator finish to load |
| `lig_log_generator_inserttable` | Bool | true | Print log when somemone instert a tablet in a generator |
| `lig_log_generator_unlock` | Bool | true | Print log when someone unlock a generator |
