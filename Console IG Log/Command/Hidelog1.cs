﻿using Console_IG_Log;
using Smod2.API;
using Smod2.Commands;

namespace Command
{
    internal class Hidelog : ICommandHandler
    {
        private ConsoleIG consoleIG;

        public Hidelog(ConsoleIG consoleIG)
        {
            this.consoleIG = consoleIG;
        }

        public string GetCommandDescription()
        {
            return "Hide the log in the console.";
        }

        public string GetUsage()
        {
            return "HIDELOG / HL";
        }

        public string[] OnCall(ICommandSender sender, string[] args)
        {
            if (sender is Player p)
            {
                if (ConsoleIG.hideloglist.Contains(p.SteamId))
                {
                    ConsoleIG.hideloglist.Remove(p.SteamId);
                    return new string[] { "You can see the log now." };
                }
                else
                {
                    ConsoleIG.hideloglist.Add(p.SteamId);
                    return new string[] { "You can't see the log now." };
                }
            }
            return new string[] { "You aren't a player" };
        }
    }
}