﻿using System;
using System.Collections.Generic;
using System.Linq;
using Smod2;
using Smod2.Attributes;
using Smod2.API;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using System.Globalization;
using static Console_IG_Log.IpInfoAPI;

namespace Console_IG_Log
{
    [PluginDetails(
        author = "Flo - Fan, Quelq1",
        description = "Print log in your console",
        id = "flo.lig",
        name = "Log In Game",
        version = "1.1.1",
        SmodMajor = 3,
        SmodMinor = 4,
        SmodRevision = 0, 
        configPrefix = "lig",
        langFile = "LogInGame"
        )]
    public class ConsoleIG : Plugin
    {

        public override void OnDisable()
        {
            this.Info("Plugin @#fg=Red;Disabled");
        }

        public override void OnEnable()
        {
            this.Info("Plugin @#fg=Green;Enabled");
        }

        public static List<string> hideloglist = new List<string>();

        public override void Register()
        {

            // Main Config

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_rank", new string[] { "owner" }, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_steamid", new string[] { string.Empty }, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_steamid_badge", true, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_steamid_badge_name", "The Log Shower", true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_steamid_badge_color", "red", true, "Set rank able to show log"));

            // In Game

            // Use in RoundHandler.cs

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_teamrespawn", true, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_intercom", true, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_roundstart", true, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_roundend", true, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_lczdecont", true, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_warhead_start", true, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_warhead_stop", true, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_warhead_explode", true, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_decide_team_respawn", true, true, "Set rank able to show log"));

                // Use in ServerHandler.cs

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_connect", true, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_join", true, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_disconnect", true, true, "Set rank able to show log"));

                // Use in PlayerHandler.cs

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_kill", true, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_grenade", true, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_command", true, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_ban", true, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_handcuff", true, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_elevator", true, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_escape", true, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_door", true, false, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_door_secure", true, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_authcheck", true, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_zombie", true, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_callcommand", false, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_player_spawn", true, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_pd_die", true, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_pd_enter", true, true, "Set rank able to show log"));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_pd_exit", true, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_lure", true, true, "Set rank able to show log"));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_contain_106", true, true, "Set rank able to show log"));

            // Use in SCP079Handler.cs

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_079_door", false, true, ""));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_079_lock", false, true, ""));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_079_elevator", false, true, ""));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_079_teslagate", false, true, ""));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_079_addexp", false, true, ""));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_079_levelup", false, true, ""));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_079_unlockdoors", false, true, ""));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_079_camerateleport", false, true, ""));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_079_elevatorteleport", false, true, ""));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_079_startspeaker", false, true, ""));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_079_stopspeaker", false, true, ""));

            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_generator_access", true, true, ""));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_generator_ejectablet", true, true, ""));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_generator_finish", true, true, ""));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_generator_inserttablet", true, true, ""));
            this.AddConfig(new Smod2.Config.ConfigSetting("lig_log_generator_unlock", true, true, ""));


            // Event Handler Declaration

            this.AddEventHandlers(new RoundHandler(this), Smod2.Events.Priority.Normal);
            this.AddEventHandlers(new ServerHandler(this), Smod2.Events.Priority.Normal);
            this.AddEventHandlers(new PlayerHandler(this), Smod2.Events.Priority.Normal);
            this.AddEventHandlers(new SCP079Handler(this), Smod2.Events.Priority.Normal);

            // Command Declaration

            this.AddCommands(new string[] { "hidelog", "hl" }, new Command.Hidelog(this));
            //this.AddCommands(new string[] { "ipinfo", "ipi" }, new Command.IpInfoCommands(this));

            // Translation

            this.AddTranslation(new Smod2.Lang.LangSetting("onConnect", "A player is attempting to connect...", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onDisconnect", "A player has disconnected.", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onJoin", "[player] <color=#4F4F4F>is now connected to the server. The IP adress is coming from</color> [iplocation1] [ [iplocation2] ]", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onAdminQuery", "[user_group] [user_name] <color=#4F4F4F>has executed :</color> [query].", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onAuthCheck_deny", "[name] has tried to connect to the RA Panel.", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onAuthCheck_allow", "[user_group] [user_name] <color=#4F4F4F>is now connected to the RA pannel.</color>", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onBan_NoReason", "[victim_name] was banned [admin_group] [admin_name] during [duration] minutes.", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onBan_Reason",  "[victim_name] was banned [admin_group] [admin_name] for [reason] during [duration] minutes.", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onEscape", "[player_role] [player_name] <color=#4F4F4F>has escaped from the Facility.</color>", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onContain106", "[player_role] [player_name] <color=#4F4F4F>has contained 106 successfully.</color>", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onDoorAccess_deny", "[player_role] [player_name] <color=#4F4F4F>has opened a door.</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onDoorAccess_allow", "[player_role] [player_name] <color=#4F4F4F>has tried to open a door.</color>", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onDoorAccess_secure_deny", "[player_role] [player_name] <color=#4F4F4F>has tried to open the door</color> [door]<color=#4F4F4F>.</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onDoorAccess_secure_allow", "[player_role] [player_name] <color=#4F4F4F>has opened the door</color> [door]<color=#4F4F4F>.</color>", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onElevator", "[player_role] [player_name] <color=#4F4F4F>has used an elevator.</color> [name]", "LogInGame")); //TODO: Name Elevator

            this.AddTranslation(new Smod2.Lang.LangSetting("onHandcuff", "[player_role] [player_name] <color=#4F4F4F>has handcuffed</color> [victim_role] [victim_name].", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onLure", "[player_role] [player_name] <color=#4F4F4F>has broken his Femur to recontain SCP 106.</color>", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onDie_TeamKill", "[killer_role] [killer_name] ([killer_steamid]) has teamkilled [player_role] [player_name] ([player_steamid]) using [damage].", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onDie_Kill", "[killer_role] [killer_name] ([killer_steamid]) <color=#4F4F4F>has killed</color> [player_role] [player_name] ([player_steamid]) <color=#4F4F4F>using</color> [damage].", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onDie_Suicid", "[killer_role] [killer_name] ([killer_steamid]) <color=#4F4F4F>has suicided himself using</color> [damage].", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onPd_die", "[player_role] [player_name] <color=#4F4F4F>has died in the Pocked Dimention.</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onPd_enter", "[player_role] [player_name] <color=#4F4F4F>has entered in the Pocked Dimention.</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onPd_exit", "[player_role] [player_name] <color=#4F4F4F>has escaped from the Pocked Dimention.</color>", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onSpawn", "[player_name] <color=#4F4F4F>has become a</color> [role]", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onGrenade_flash", "[player_role] [player_name] <color=#4F4F4F>has thrown a</color> FlashBang.", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onGrenade_frag", "[player_role] [player_name] <color=#4F4F4F>has thrown a</color> FragGrenade.>", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onTeamRespawnQueue", "<color=#4F4F4F>The Team Respawn Queue is :</color> [queue]", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onDecont", "<color=#4F4F4F>The LCZ Decontamination is now ON.</color>", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onWarhead_detonate", "<color=#4F4F4F>The Omega Warhead has exploded the inside from the Facility.</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onWarhead_start", "[activator_role] [activator_name] <color=#4F4F4F>has activated the Warhead ! The explosion will occur in T - [time_left] seconds !</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onWarhead_stop", "[activator_role] [activator_name] <color=#4F4F4F>has canceled the Warhead !</color>", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onIntercom", "[player_role] [player_name] <color=#4F4F4F>is speaking at intercom.</color>", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onRoundEnd_1", "------ Round End ------", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onRoundEnd_2", "<color=#FF4D00>Class D espaced : [number]</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onRoundEnd_3", "<color=#FF4D00>Class D alive : [alive]/[start]</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onRoundEnd_4", "<color=#FFFF6A>Scientist espaced : [number]</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onRoundEnd_5", "<color=#FFFF6A>Scientist alive : [alive]/[start]</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onRoundEnd_6", "<color=#C10000>SCP alive : [alive]/[start]</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onRoundEnd_7", "<color=#C10000>SCP have killed [number] player(s)</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onRoundEnd_8", "Grenades have killed [number] player(s)", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onRoundEnd_9", "<color=#00BAFF>MTF alive : [alive]</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onRoundEnd_10", "The Round has durated : [time] minutes", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onRoundEnd_11", "-----------------------", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onRoundStart", "----- New round has started -----", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onTeamRespawn_CI", "<color=#4F4F4F>Chaos Insurgency has entered into the facility.</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onTeamRespawn_MTF", "<color=#4F4F4F>The MTF is here to assurate the security of the facility.</color>", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onRecallZombie", "[player_role] [player_name] <color=#4F4F4F>has cured the player [victim_name].", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onCallCommand", "[group] [name] <color=#4F4F4F>has executed the command</color> [command] <color=#4F4F4F>in the console</color>", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("on079AddExp", "<color=#C10000>SCP 079</color> [player_name] <color=#4F4F4F>has won</color> [amount_xp] <color=#4F4F4F>XPs with</color> [type]", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("on079CameraTeleport", "<color=#C10000>SCP 079</color> [player_name] <color=#4F4F4F>has teleported himself to a camera.</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("on079DoorOpen", "<color=#C10000>SCP 079</color> [player_name] <color=#4F4F4F>has opened a door (</color>[door_name]<color=#4F4F4F)</color>.", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("on079DoorClose", "<color=#C10000>SCP 079</color> [player_name] <color=#4F4F4F>has closed a door (</color>[door_name]<color=#4F4F4F)</color>.", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("on079Elevator", "<color=#C10000>SCP 079</color> [player_name] <color=#4F4F4F>has called an elevator</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("on079ElevatorTeleport", "<color=#C10000>SCP 079</color> [player_name] <color=#4F4F4F>has teleported himself with an elevator.</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("on079LevelUp", "<color=#C10000>SCP 079</color> [player_name] <color=#4F4F4F>has leveled up.</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("on079Lock", "<color=#C10000>SCP 079</color> [player_name] <color=#4F4F4F>has locked a door (</color>[door_name]<color=#4F4F4F)</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("on079Lockdown", "<color=#C10000>SCP 079</color> [player_name] <color=#4F4F4F>has locked a room in [zone]</color>.", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("on079StartSpeaker", "<color=#C10000>SCP 079</color> [player_name] <color=#4F4F4F>has begun to speak in [zone]</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("on079StopSpeaker", "<color=#C10000>SCP 079</color> [player_name] <color=#4F4F4F>has finished to speak in [zone]</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("on079TeslaGate", "<color=#C10000>SCP 079</color> [player_name] <color=#4F4F4F>has activated a tesla gate</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("on079UnlockDoors", "<color=#C10000>SCP 079</color> [player_name] <color=#4F4F4F>has unlocked doors</color>", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("onGeneratorAccessOpen", "[class_name] [player_name] <color=#4F4F4F>has opened a generator</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onGeneratorAccessClose", "[class_name] [player_name] <color=#4F4F4F>has closed a generator</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onGeneratorInsertTablet", "[class_name] [player_name] <color=#4F4F4F>has ejected a tablet in a generator</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onGeneratorFinish", "<color=#4F4F4F>A generator has finished.</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onGeneratorEjectTablet", "[class_name] [player_name] <color=#4F4F4F>has inserted a tablet. He left [time] <color=#4F4F4F>seconds before the generator will finish.</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("onGeneratorUnlock", "[class_name] [player_name] <color=#4F4F4F>has unlocked a generator.</color>", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("UNASSIGNED", "UNASSIGNED", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("SCP_173", "<color=#C10000>SCP 173</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("CLASSD", "<color=#FF4D00>Class D</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("SPECTATOR",  "<color=#6E6E6E>Spectator</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("NTF_SCIENTIST", "<color=#00BAFF>NTF Scientist</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("SCP_106", "<color=#C10000>SCP 106</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("SCP_049", "<color=#C10000>SCP 049</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("SCIENTIST", "<color=#FFFF6A>Scientist</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("SCP_079", "<color=#C10000>SCP 079</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("CHAOS_INSURGENCY", "<color=#1E5400>Chaos Insurgency</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("SCP_096", "<color=#C10000>SCP 096</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("SCP_049_2", "<color=#C10000>SCP 049-2</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("ZOMBIE", "<color=#C10000>SCP 049-2</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("NTF_LIEUTENANT", "<color=#00BAFF>NTF Lieutenant</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("NTF_COMMANDER", "<color=#0048FF>NTF Commander</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("NTF_CADET", "<color=#6DDAFF>NTF Cadet</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("TUTORIAL", "<color=#5CFF00>Tutorial</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("FACILITY_GUARD", "<color=#6E6E6E>Facility Guard</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("SCP_939_53", "<color=#C10000>SCP 939-53</color>", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("SCP_939_89", "<color=#C10000>SCP 939-89</color>", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("NULL", "NULL", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("JANITOR_KEYCARD", "Janitor Keycard", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("SCIENTIST_KEYCARD", "Scientist Keycard", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("MAJOR_SCIENTIST_KEYCARD", "Major Scientist Keycard", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("ZONE_MANAGER_KEYCARD", "Zone Manager Keycard", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("GUARD_KEYCARD", "Guard Keycard", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("SENIOR_GUARD_KEYCARD", "Senior Guard Keycard", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("CONTAINMENT_ENGINEER_KEYCARD", "Containment Engineer Keycard", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("MTF_LIEUTENANT_KEYCARD", "MTF Lieutenant Keycard", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("MTF_COMMANDER_KEYCARD", "MTF Commander Keycard", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("FACILITY_MANAGER_KEYCARD", "Facility Manager Keycard", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("CHAOS_INSURGENCY_DEVICE", "Chaos KeyCard", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("O5_LEVEL_KEYCARD", "05 KeyCard", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("RADIO", "Radio", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("COM15", "Com15 Pistol", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("MEDKIT", "Medkit", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("FLASHLIGHT", "Flashlight", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("MICROHID", "MicroHID", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("COIN", "Coin", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("CUP", "Cup", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("WEAPON_MANAGER_TABLET", "Weapon Manager Tablet", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("E11_STANDARD_RIFLE", "Epsilon-11 Standard Rifle", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("P90", "P-90", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("DROPPED_5", "Epsilon ammo", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("MP4", "MP7", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("LOGICER", "Logicer", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("FRAG_GRENADE", "Frag Grenade", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("FLASHBANG", "Flash Grenade", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("DISARMER", "Disarmer", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("DROPPED_7", "MP7/Logicer ammo", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("DROPPED_9", "Pistol/P90/USP ammo", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("USP", "USP", "LogInGame"));

            this.AddTranslation(new Smod2.Lang.LangSetting("NONE", "None", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("LURE", "Lure", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("NUKE", "Nuke", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("WALL", "Wall", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("DECONT", "Decontamination", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("TESLA", "Tesla", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("FALLDOWN", "Falldown", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("FLYING", "Fly", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("CONTAIN", "Contain", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("POCKET", "Pocket Dimension", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("RAGDOLLLESS", "Ragdollless", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("MP7", "MP7", "LogInGame"));
            this.AddTranslation(new Smod2.Lang.LangSetting("FRAG", "Frag Grenade", "LogInGame"));

        }

        
    }

    public static class ip
    {
        public static string[] GetUserCountryByIp(string ip)
        {
            string symbol = null;
            string name = null;
            IpInfo ipInfo = new IpInfo();
            try
            {
                string info = new WebClient().DownloadString("http://ipinfo.io/" + ip + "/geo");
                ipInfo = JsonConvert.DeserializeObject<IpInfo>(info);
                RegionInfo ri = new RegionInfo(ipInfo.Country);
                name = ri.DisplayName;
                symbol = ri.TwoLetterISORegionName;
            }
            catch (Exception)
            {
                ipInfo.Country = null;
            }

            return new string[] { name, symbol };
        }
    }

    // Check if the player has got the rank


    public static class perm
    {
        public static bool CheckPerm(Player p)
        {
            string[] ranklist = Smod2.ConfigManager.Manager.Config.GetListValue("lig_rank",new string[] { "owner" }, false);
            string[] steamidlist = Smod2.ConfigManager.Manager.Config.GetListValue("lig_steamid", new string[] { string.Empty });

            if(ranklist.Contains(p.GetRankName()) || ranklist.Contains(p.GetUserGroup().Name) && ConsoleIG.hideloglist.Contains(p.SteamId) == false)
                return true;
            if (steamidlist.Contains(p.SteamId))
                return true;

            return false;
        }
    }

    public static class PlayerName
    {
        public static Player GetPlayer(string name)
        {
            foreach(Player p in PluginManager.Manager.Server.GetPlayers())
            {
                if (name.ToLower().Contains(p.Name.ToLower()))
                {
                    return p;
                }
                else
                {
                    continue;
                }
            }
            return null;
        }
    }
}

