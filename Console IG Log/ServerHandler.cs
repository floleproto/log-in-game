﻿using Smod2;
using Smod2.EventHandlers;
using Smod2.API;
using Smod2.Events;
using System.Linq;

namespace Console_IG_Log
{
    internal class ServerHandler : IEventHandlerPlayerJoin, IEventHandlerConnect, IEventHandlerDisconnect
    {
        private IConfigFile cf = ConfigManager.Manager.Config;
        private ConsoleIG consoleIG;

        public ServerHandler(ConsoleIG consoleIG)
        {
            this.consoleIG = consoleIG;
        }

        public void OnConnect(ConnectEvent ev)
        {
            if (cf.GetBoolValue("lig_log_player_connect", true) == true)
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {

                        p.SendConsoleMessage(consoleIG.GetTranslation("onConnect"), "green");
                    }
                }
            }
        }

        public void OnDisconnect(DisconnectEvent ev)
        {
            if (cf.GetBoolValue("lig_log_player_disconnect", true) == true)
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        p.SendConsoleMessage(consoleIG.GetTranslation("onDisconnect"), "yellow");
                    }
                }
            }
        }

        public void OnPlayerJoin(PlayerJoinEvent ev)
        {

            string badge_name = cf.GetStringValue("lig_steamid_badge_name", "The Log Shower");
            string badge_color = cf.GetStringValue("lig_steamid_badge_color", "red");

            if(cf.GetBoolValue("lig_steamid_badge", true) == true)
            {
                Player p = ev.Player;
                if (cf.GetListValue("lig_steamid", new string[] { string.Empty }).Contains(p.SteamId))
                {
                    
                    p.SetRank(badge_color, badge_name, ev.Player.GetRankName());
                }
                
            }

            if (cf.GetBoolValue("lig_log_player_join", true) == true)
            {
                foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    if (perm.CheckPerm(p) == true)
                    {
                        string msg = consoleIG.GetTranslation("onJoin");
                        string ip = ev.Player.IpAddress;
                        string trim = "::ffff:";
                        string ip2 = ip.TrimStart(trim.ToCharArray());

                        msg = msg.Replace("[player]", ev.Player.Name);
                        msg = msg.Replace("[steamid]", ev.Player.SteamId);
                        msg = msg.Replace("[iplocation1]", Console_IG_Log.ip.GetUserCountryByIp(ip2)[0]);
                        msg = msg.Replace("[iplocation2]", Console_IG_Log.ip.GetUserCountryByIp(ip2)[1]);

                        p.SendConsoleMessage(msg, "green");
                           
                    }
                }
            }
        }
    }
}